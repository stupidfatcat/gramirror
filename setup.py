from setuptools import setup

config = {
    'description': 'Instagram mirror service. It takes your Instagram posts and mirrors them with the attached media to Twitter.',
    'author': 'Shelby Shum',
    'url': 'https://gitlab.com/ss2501/gramirror',
    'download_url': 'https://gitlab.com/ss2501/gramirror.git',
    'author_email': 'sshum00@gmail.com',
    'version': '0.0.1',
    'install_requires': [
        'click',
        'python-instagram',
        'tweepy',
    ],
    'packages': ['gramirror'],
    'scripts': [],
    'name': 'gramirror',
}

setup(**config)
